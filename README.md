# lsr

Lists contents of a given folder.


## Installation

`npm install`


## Tests

Ensure `test/mock/foo/bar/baz` directory is present before running test.

`npm test`


## Example

Running example code:

* Ensure `test/mock/foo/bar/baz` directory is present before running test.

* Run: `node example`

*  Returns
```
{ filenames:
   [ '/path/lsr/test/mock/foo/f2.txt',
     '/path/lsr/test/mock/foo/f1.txt',
     '/path/lsr/test/mock/foo/bar/bar2.txt',
     '/path/lsr/test/mock/foo/bar/bar1.txt' ],
  dirnames:
   [ '/path/lsr/test/mock/foo',
     '/path/lsr/test/mock/foo/bar',
     '/path/lsr/test/mock/foo/bar/baz' ] }
```

## Usage

```
const getFolderContents = require('lsr'); // path to module
console.log(getFolderContents('./test/mock/foo'));
// returns {filenames: [], dirnames:[]} data structure.
```
