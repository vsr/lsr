const fs = require('fs');
const path = require('path');
const FileResponse = require('./fileresponse');

class File {
  constructor(filepath) {
    this.filepath = filepath;
    this.realpath = path.resolve(filepath);
  }

  get stat() {
    //cache the file stat object
    return (this._fileStat || (this._fileStat = fs.statSync(this.realpath)));
  }

  get isFile() {
    return this.stat.isFile();
  }

  get isDirectory() {
    return this.stat.isDirectory();
  }

  getFolderContents() {
    const dirData = fs.readdirSync(this.realpath);
    let response = new FileResponse();
    let filepath, file, fileStat;
    if(this.isDirectory) {
      response.addDirectory(this.realpath);
    }
    for (var i = dirData.length - 1; i >= 0; i--) {
      filepath = path.resolve(this.realpath, dirData[i]);
      file = new File(filepath);
      if(file.isFile){
        response.addFile(filepath);
      }
      else if (file.isDirectory){
        // recurse and merge response for a directory
        response.merge(file.getFolderContents());
      }
    }
    return response;
  }

}

module.exports = File;