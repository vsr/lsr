class FileResponse {
  constructor(){
    this.response = { filenames: [], dirnames: [] };
  }

  addFile(filename) {
    this.response.filenames.push(filename);
  }

  addDirectory(dirname) {
    this.response.dirnames.push(dirname);
  }

  merge(source) {
    if(source.response.filenames) {
      this.response.filenames = this.response.filenames.concat(source.response.filenames);
    }
    if(source.response.dirnames) {
      this.response.dirnames = this.response.dirnames.concat(source.response.dirnames);
    }
  }

}

module.exports = FileResponse;