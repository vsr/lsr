const File = require('./lib/file');

function getFolderContents(filepath) {
  const file = new File(filepath);
  return file.getFolderContents().response;
}

module.exports = getFolderContents;