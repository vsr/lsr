const expect = require('chai').expect;
const fs = require('fs');

const File = require('../lib/file');
const FileResponse = require('../lib/fileresponse');

describe("File", function(){

  const mockFoo = './test/mock/foo';
  const mockDir = './test/mock';
  const mockFile = './test/mock/foo1.txt';

  it("should be a constructor", function(){
    expect(File).to.be.a('function');
    expect(File.prototype.constructor).to.be.a('function');
    expect(File.prototype.constructor).to.equal(File);
  });

  it("should return an instance of File", function(){
    const file = new File(mockDir);
    expect(file).to.be.an.instanceof(File);
  });

  it("should have realpath defined", function(){
    const file = new File(mockDir);
    expect(file.realpath).to.be.a('string');
  });

  it("should have stat object", function(){
    const file = new File(mockDir);
    expect(file.stat).to.be.an.instanceof(fs.Stats);
  });

  describe("File.isDirectory", function(){

    it("should return true for directory", function(){
      const file = new File(mockDir);
      expect(file.isDirectory).to.be.true;
    });

    it("should return false for file", function(){
      const file = new File(mockFile);
      expect(file.isDirectory).to.not.be.true;
    });

  });

  describe("File.isFile", function(){

    it("should return true for file", function(){
      const file = new File(mockFile);
      expect(file.isFile).to.be.true;
    });

    it("should return false for directory", function(){
      const file = new File(mockDir);
      expect(file.isFile).to.not.be.true;
    });

  });

  describe("getFolderContents()", function(){

    it("should return FileResponse instance", function(){
      const file = new File(mockDir);
      expect(file.getFolderContents()).to.be.an.instanceof(FileResponse);
    });

    it("should return correct filenames array", function(){
      const file = new File(mockFoo);
      expect(file.getFolderContents().response.filenames).to.have.lengthOf(4);
    });

    it("should return correct dirnames array", function(){
      const file = new File(mockFoo);
      expect(file.getFolderContents().response.dirnames).to.have.lengthOf(3);
    });

  });


});