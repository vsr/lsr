const expect = require('chai').expect;

const FileResponse = require('../lib/fileresponse');

describe("FileResponse", function(){

  it("should be a constructor", function(){
    expect(FileResponse).to.be.a('function');
    expect(FileResponse.prototype.constructor).to.be.a('function');
    expect(FileResponse.prototype.constructor).to.equal(FileResponse);
  });

  it("should return an instance of FileResponse", function(){
    const fileResponse = new FileResponse();
    expect(fileResponse).to.be.an.instanceof(FileResponse);
  });

  it("should initialize empty response", function(){
    const fileResponse = new FileResponse();
    expect(fileResponse.response).to.have.all.keys(['filenames', 'dirnames']);
    expect(fileResponse.response.filenames).to.be.empty;
    expect(fileResponse.response.dirnames).to.be.empty;
  });

  describe("addFile()", function(){

    it("should update the filenames", function(){
      const fileResponse = new FileResponse();
      const testFile = 'test.txt';
      fileResponse.addFile(testFile);
      expect(fileResponse.response.filenames).to.have.lengthOf(1);
      expect(fileResponse.response.filenames).to.include(testFile);
    });

    it("should update the filenames on multiple calls", function(){
      const fileResponse = new FileResponse();
      const testFiles = ['test.txt', 'test2.txt', 'test3.txt'];
      for(let i in testFiles){
        fileResponse.addFile(testFiles[i]);
      }
      expect(fileResponse.response.filenames).to.have.lengthOf(3);
      expect(fileResponse.response.filenames).to.include.members(testFiles);
    });

  });

  describe("addDirectory()", function(){

    it("should update the dirnames", function(){
      const fileResponse = new FileResponse();
      const testDir = 'test';
      fileResponse.addDirectory(testDir);
      expect(fileResponse.response.dirnames).to.have.lengthOf(1);
      expect(fileResponse.response.dirnames).to.include(testDir);
    });

    it("should update the dirnames on multiple calls", function(){
      const fileResponse = new FileResponse();
      const testDirs = ['test', 'test2', 'test3'];
      for(let i in testDirs){
        fileResponse.addDirectory(testDirs[i]);
      }
      expect(fileResponse.response.dirnames).to.have.lengthOf(3);
      expect(fileResponse.response.dirnames).to.include.members(testDirs);
    });

  });

  describe("merge()", function(){

    it("should merge filenames on empty target", function(){
      const source = new FileResponse();
      const target = new FileResponse();
      const filenames = ["hello.txt"];
      source.addFile(filenames[0]);

      target.merge(source);
      expect(target.response.filenames).to.have.lengthOf(1);
      expect(target.response.filenames).to.include(filenames[0]);
    });

    it("should merge filenames on empty source", function(){
      const source = new FileResponse();
      const target = new FileResponse();
      const filenames = ["hello.txt"];
      target.addFile(filenames[0]);

      target.merge(source);
      expect(target.response.filenames).to.have.lengthOf(1);
      expect(target.response.filenames).to.include(filenames[0]);
    });

    it("should merge filenames on non-empty source and target", function(){
      const source = new FileResponse();
      const target = new FileResponse();
      const filenames = ["hello.txt", "world.txt"];
      target.addFile(filenames[0]);
      source.addFile(filenames[1]);

      target.merge(source);
      expect(target.response.filenames).to.have.lengthOf(2);
      expect(target.response.filenames).to.include.members(filenames);
    });

    it("should merge dirnames on empty target", function(){
      const source = new FileResponse();
      const target = new FileResponse();
      const dirnames = ["hello"];
      source.addDirectory(dirnames[0]);

      target.merge(source);
      expect(target.response.dirnames).to.have.lengthOf(1);
      expect(target.response.dirnames).to.include(dirnames[0]);
    });

    it("should merge dirnames on empty source", function(){
      const source = new FileResponse();
      const target = new FileResponse();
      const dirnames = ["hello"];
      target.addDirectory(dirnames[0]);

      target.merge(source);
      expect(target.response.dirnames).to.have.lengthOf(1);
      expect(target.response.dirnames).to.include(dirnames[0]);
    });

    it("should merge dirnames on non-empty source and target", function(){
      const source = new FileResponse();
      const target = new FileResponse();
      const dirnames = ["hello", "world"];
      target.addDirectory(dirnames[0]);
      source.addDirectory(dirnames[1]);

      target.merge(source);
      expect(target.response.dirnames).to.have.lengthOf(2);
      expect(target.response.dirnames).to.include.members(dirnames);
    });

  });

});